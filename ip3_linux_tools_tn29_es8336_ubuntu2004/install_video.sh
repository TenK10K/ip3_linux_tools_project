#!/bin/bash

sudo cp sources.list /etc/apt/sources.list
sudo chmod 644 /etc/apt/sources.list
sudo apt-get update

sudo apt-get -y install ffmpeg vlc libc-dbg

sudo cp sources.list.old /etc/apt/sources.list
sudo chmod 644 /etc/apt/sources.list
sudo apt-get update
