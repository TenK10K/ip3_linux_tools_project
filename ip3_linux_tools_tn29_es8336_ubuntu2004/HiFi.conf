SectionVerb {
	EnableSequence [
		# Disable all inputs / outputs
		cset "name='Speaker Switch' off"
		cset "name='Headphone Switch' off"
		cset "name='DAC Mono Mix Switch' off"
	]
}

If.amic {
	Condition {
		Type String
		Empty "${var:DeviceDmic}"
	}
	True.SectionDevice."Mic" {
		Comment "Analog Microphone"

		ConflictingDevice [
			"Headset"
		]

		EnableSequence [
			cset "name='Internal Mic Switch' on"
		]

		DisableSequence [
		]

		Value {
			CapturePriority 100
			CapturePCM "hw:${CardId}"
			CaptureMixerElem "ADC PGA Gain"
			CaptureMasterElem "ADC"
		}
	}
}

If.dmic {
	Condition {
		Type String
		Empty "${var:DeviceDmic}"
	}
	
	ConflictingDevice [
		"Headset"
	]
	
	False.SectionDevice."${var:DeviceDmic}" {
		Comment "Digital Microphone"

		Value {
			CapturePriority 100
			CapturePCM "hw:${CardId},1"
			If.chn {
				Condition {
					Type RegexMatch
					Regex "cfg-dmics:[34]"
					String "${CardComponents}"
				}
				True {
					CaptureChannels 4
				}
			}
			CaptureMixerElem "Dmic0"
			CaptureVolume "Dmic0 Capture Volume"
			CaptureSwitch "Dmic0 Capture Switch"
		}
	}
}

SectionDevice."Speaker" {
	Comment "Speaker"

	ConflictingDevice [
		"Headphones"
	]

	EnableSequence [
		cset "name='Speaker Switch' on"
		cset "name='Headphone Playback Volume' 3,3"
		cset "name='Right Headphone Mixer Right DAC Switch' on"
		cset "name='Left Headphone Mixer Left DAC Switch' on"
		cset "name='DAC Playback Volume' 999,999"
		cset "name='Headphone Mixer Volume' 999,999"
	]

	DisableSequence [
		cset "name='Speaker Switch' off"
	]
	Value {
		PlaybackPriority 100
		PlaybackPCM "hw:${CardId}"
		# The es8316 only has a HP-amp which is muxed to the speaker
		# or to the headpones output
		PlaybackMixerElem "Headphone Mixer"
		PlaybackMasterElem "DAC"
	}
}

SectionDevice."Headphones" {
	Comment "Headphones"

	ConflictingDevice [
		"Speaker"
	]
	
	EnableSequence [
		cset "name='Headset Mic Switch' on"
		cset "name='Headphone Playback Volume' 3,3"
		cset "name='Right Headphone Mixer Right DAC Switch' on"
		cset "name='Left Headphone Mixer Left DAC Switch' on"
		cset "name='DAC Playback Volume' 999,999"
		cset "name='Headphone Mixer Volume' 999,999"
	]
	
	Value {
		PlaybackPriority 300
		PlaybackPCM "hw:${CardId}"
		PlaybackMixerElem "Headphone Mixer"
		PlaybackMasterElem "DAC"
		JackControl "Headphone Jack"
		JackHWMute "Speaker"
	}
}

SectionDevice."Headset" {
	Comment "Headset"

	If.conflict {
		Condition {
			Type String
			Empty "${var:DeviceDmic}"
		}
		True.ConflictingDevice [
			"Mic"
		]
	}

	EnableSequence [
		cset "name='ADC PGA Gain Volume' 10"
		cset "name='ADC Capture Volume' 192"
		cset "name='Headset Mic Switch' on"
		cset "name='Digital Mic Mux' 'dmic disable'"
		cset "name='Internal Mic Switch' on"
		cset "name='Differential Mux' lin2-rin2"
	]

	DisableSequence [
	]

	Value {
		CapturePriority 300
		CapturePCM "hw:${CardId},0"
		CaptureMixerElem "ADC PGA Gain"
		CaptureMasterElem "ADC"
		JackControl "Headphone Jack"
	}
}

Include.hdmi.File "Hdmi.conf"
