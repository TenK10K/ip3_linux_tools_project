#!/bin/bash



sudo dpkg -i --force-overwrite ./audio_driver/tools/firm*.deb

sudo cp ./audio_driver/tplg/* /lib/firmware/intel/sof-tplg/
sudo cp ./audio_driver/sof-dyndbg.conf.txt /etc/modprobe.d/sof-dyndbg.conf

#echo setting up hdmi device
sudo cp -rf ./audio_driver/es8326/sof-essx8336 /usr/share/alsa/ucm2
sudo sed -i '/load-module module-suspend-on-idle/d' /etc/pulse/default.pa

sudo cp ./audio_driver/tgl_dmc_ver2_12.bin /lib/firmware/i915/tgl_dmc_ver2_12.bin

sudo cp ./sof-jsl_1.9.2.ri /lib/firmware/intel/sof/sof-jsl.ri
sudo sh -c 'echo "options snd_soc_es8326 dyndbg=+p" >> /etc/modprobe.d/blacklist.conf'

sudo dpkg -i ./deb_linux-5.18.1_20220622_1510_es8326_jb20/*.deb

sudo sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT.*=.*/GRUB_CMDLINE_LINUX_DEFAULT="quiet loglevel=0 splash"/g' /etc/default/grub

sudo update-grub


