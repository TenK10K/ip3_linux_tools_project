#!/bin/bash


sh -c 'echo "\n[Install]\nWantedBy=multi-user.target\nAlias=rc-local.service\n" >> /lib/systemd/system/rc-local.service'

cp rc.local /etc/rc.local
chmod 755 /etc/rc.local
