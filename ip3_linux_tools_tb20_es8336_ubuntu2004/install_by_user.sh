#!/bin/bash



sudo dpkg -i --force-overwrite ./audio_driver/tools/firm*.deb

sudo cp ./audio_driver/tplg/* /lib/firmware/intel/sof-tplg/
sudo cp ./audio_driver/sof-dyndbg.conf.txt /etc/modprobe.d/sof-dyndbg.conf

#echo setting up hdmi device
sudo cp -rf ./audio_driver/sof-essx8336 /usr/share/alsa/ucm2
sudo sed -i '/load-module module-suspend-on-idle/d' /etc/pulse/default.pa

sudo cp ./audio_driver/tgl_dmc_ver2_12.bin /lib/firmware/i915/tgl_dmc_ver2_12.bin

sudo cp /lib/firmware/intel/sof-tplg/sof-tgl-es8336-dmic2ch-ssp0.tplg /lib/firmware/intel/sof-tplg/sof-tgl-es8336-dmic1ch-ssp0.tplg
sudo cp ./HiFi.conf /usr/share/alsa/ucm2/sof-essx8336/HiFi.conf

sudo dpkg -i ./deb_linux-5.18_20220525_2122_es8316_tb20/*.deb

sudo sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT.*=.*/GRUB_CMDLINE_LINUX_DEFAULT="quiet loglevel=0 splash"/g' /etc/default/grub
#sudo sed -i 's/enabled.*=.*/enabled=1/g' /etc/default/apport

sudo update-grub


