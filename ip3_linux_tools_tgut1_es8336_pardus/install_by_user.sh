#!/bin/bash

sudo dpkg -i --force-overwrite ./audio_driver/tools/firm*.deb

sudo cp ./audio_driver/tplg/* /lib/firmware/intel/sof-tplg/
sudo cp ./audio_driver/sof-dyndbg.conf.txt /etc/modprobe.d/sof-dyndbg.conf

#echo setting up hdmi device
sudo cp -rf ./audio_driver/sof-essx8336 /usr/share/alsa/ucm
sudo sed -i '/load-module module-suspend-on-idle/d' /etc/pulse/default.pa

sudo cp ./audio_driver/tgl_dmc_ver2_12.bin /lib/firmware/i915/tgl_dmc_ver2_12.bin

sudo dpkg -i ./deb_linux-5.18.1_20220531_1942_tgut1_es8336/*.deb

sudo update-grub

sudo apt-mark hold linux-image-5.18.1-ip3-0531 linux-headers-5.18.1-ip3-0531

