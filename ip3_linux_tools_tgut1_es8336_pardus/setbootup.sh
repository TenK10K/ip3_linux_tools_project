#!/bin/bash

#sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT.*=.*/GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"/g' /etc/default/grub

#sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT.*=.*/GRUB_CMDLINE_LINUX_DEFAULT="quiet splash nomodeset"/g' /etc/default/grub

sed -i '/\[Install\]/d' /lib/systemd/system/rc-local.service
sed -i '/WantedBy=multi-user.target/d' /lib/systemd/system/rc-local.service
sed -i '/Alias=rc-local.service/d' /lib/systemd/system/rc-local.service

sh -c 'echo "\n[Install]\nWantedBy=multi-user.target\nAlias=rc-local.service\n" >> /lib/systemd/system/rc-local.service'

cp rc.local /etc/rc.local
chmod 755 /etc/rc.local
