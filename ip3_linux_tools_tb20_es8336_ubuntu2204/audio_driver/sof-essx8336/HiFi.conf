SectionVerb {
	EnableSequence [
		#disdevall ""
		# Disable all inputs / outputs
		#   (may be duplicated with disdevall)
		cset "name='Left Headphone Mixer Left DAC Switch' on"
		cset "name='Right Headphone Mixer Right DAC Switch' on"
	]
}

If.amic {
	Condition {
		Type String
		Empty "${var:DeviceDmic}"
	}
	True.SectionDevice."Mic" {
		Comment "Analog Microphone"

		ConflictingDevice [
			"Headset"
		]

		EnableSequence [
			cset "name='Internal Mic Switch' on"
		]

		DisableSequence [

				]

		Value {
			CapturePriority 100
			CapturePCM "hw:${CardId}"
			CaptureMixerElem "ADC PGA Gain"
			CaptureMasterElem "ADC"
		}
	}
}

If.dmic {
	Condition {
		Type String
		Empty "${var:DeviceDmic}"
	}
	False.SectionDevice."${var:DeviceDmic}" {
		Comment "Digital Microphone"

		Value {
			CapturePriority 100
			CapturePCM "hw:${CardId},1"
			If.chn {
				Condition {
					Type RegexMatch
					Regex "cfg-dmics:[34]"
					String "${CardComponents}"
				}
				True {
					CaptureChannels 4
				}
			}
			CaptureMixerElem "Dmic0"
			CaptureVolume "Dmic0 Capture Volume"
			CaptureSwitch "Dmic0 Capture Switch"
		}
	}
}

SectionDevice."Speaker" {
	Comment "Speakers"

	ConflictingDevice [
		"Headphones"
	]

	EnableSequence [
		cset "name='Speaker Switch' on"
	]

	DisableSequence [
		cset "name='Speaker Switch' off"
	]
	Value {
		PlaybackPriority 100
		PlaybackPCM "hw:${CardId}"
		# The es8316 only has a HP-amp which is muxed to the speaker
		# or to the headpones output
		PlaybackMixerElem "Headphone Mixer"
		PlaybackMasterElem "DAC"
	}
}

SectionDevice."Headphones" {
	Comment "Headphones"

	ConflictingDevice [
		"Speaker"
	]

	Value {
		PlaybackPriority 300
		PlaybackPCM "hw:${CardId}"
		PlaybackMixerElem "Headphone Mixer"
		PlaybackMasterElem "DAC"
		JackControl "Headphone Jack"
		JackHWMute "Speaker"
	}
}

SectionDevice."Headset" {
	Comment "Headset Microphone"

	If.conflict {
		Condition {
			Type String
			Empty "${var:DeviceDmic}"
		}
		True.ConflictingDevice [
			"Mic"
		]
	}

	EnableSequence [
		cset "name='Headset Switch' on"
	]

	DisableSequence [
		cset "name='Headset Switch' on"
	]

	Value {
		CapturePriority 300
		CapturePCM "hw:${CardId}"
		CaptureMixerElem "ADC PGA Gain"
		CaptureMasterElem "ADC"
		JackControl "Headset Mic Jack"
	}
}

Include.hdmi.File "/Intel/sof-essx8336/Hdmi.conf"
