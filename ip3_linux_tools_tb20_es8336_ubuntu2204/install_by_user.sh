#!/bin/bash


sudo dpkg -i --force-overwrite ./audio_driver/firm*.deb

sudo cp ./audio_driver/tplg/* /lib/firmware/intel/sof-tplg/
sudo cp ./audio_driver/tplg/sof-glk-es8336-ssp0.tplg /lib/firmware/intel/sof-tplg/sof-tgl-es8336-ssp0.tplg
sudo cp ./audio_driver/sof-jsl.ri  /lib/firmware/intel/sof/

sudo cp ./audio_driver/sof-dyndbg.conf.txt /etc/modprobe.d/sof-dyndbg.conf

#FOR 22.04 syntax4 echo setting up hdmi device
sudo cp -rf ./audio_driver/sof-essx8336 /usr/share/alsa/ucm2/conf.d
sudo cp -rf ./audio_driver/sof-essx8336 /usr/share/alsa/ucm2/Intel
sudo sed -i '/load-module module-suspend-on-idle/d' /etc/pulse/default.pa

###for tb20
sudo cp /lib/firmware/intel/sof-tplg/sof-tgl-es8336-dmic2ch-ssp0.tplg /lib/firmware/intel/sof-tplg/sof-tgl-es8336-dmic1ch-ssp0.tplg
sudo cp ./audio_driver/HiFi.conf /usr/share/alsa/ucm2/conf.d/sof-essx8336/HiFi.conf
sudo cp ./audio_driver/HiFi.conf /usr/share/alsa/ucm2/Intel/sof-essx8336/HiFi.conf

sudo dpkg -i ./deb_linux-5.18.1_20220623_1750_es8336_tb20_ubuntu2204/*.deb

sudo sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT.*=.*/GRUB_CMDLINE_LINUX_DEFAULT="quiet loglevel=0 splash"/g' /etc/default/grub

#sudo sed -i 's/enabled.*=.*/enabled=1/g' /etc/default/apport

sudo update-grub


